FROM node:16.18.0-alpine3.16 as dev

WORKDIR /usr/src/app

COPY package*.json ./

COPY src/prisma/schema.prisma ./src/prisma/schema.prisma

RUN apk update && apk add g++ make python3 openssl

RUN npm install glob yarn rimraf

RUN yarn

CMD npx prisma db push && yarn dev
