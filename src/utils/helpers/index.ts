import { createHash } from 'crypto';
import { readFileSync } from 'fs';

export const sha = (data) => createHash('sha256').update(data).digest('hex');

export function readJsonFile<T = unknown>(path: string) {
  try {
    const file = readFileSync(path, 'utf8');
    return JSON.parse(file) as T;
  } catch (error) {
    return null;
  }
}
