import { Injectable, PipeTransform } from '@nestjs/common';

@Injectable()
export class TakeLimit implements PipeTransform {
  constructor(private limit: number = 100) {}
  transform(value: any) {
    if (!value?.take || value?.take > this.limit) {
      value.take = this.limit;
    }
    return value;
  }
}
