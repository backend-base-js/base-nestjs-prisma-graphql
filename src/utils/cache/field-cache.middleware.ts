import { FieldMiddleware, MiddlewareContext, NextFn } from '@nestjs/graphql';

export const fieldCacheMiddleware: FieldMiddleware = async (
  ctx: MiddlewareContext,
  next: NextFn
) => {
  const { prismaService, cache } = ctx.context;
  const key = `${ctx.info.path.typename}:${ctx.source.id}:${ctx.info.path.key}`;
  const type = ctx.info.returnType['ofType']['name'];
  let result;
  const keys = await cache
    .get(key)
    .then((res) => JSON.parse(res))
    .catch(() => undefined);
  if (keys) {
    if (Array.isArray(keys)) {
      result = (
        await Promise.all(
          result.map(async (k) => {
            let r = await cache
              .get(k)
              .then((res) => JSON.parse(res))
              .catch(() => undefined);
            if (!r) {
              const [model, id] = k.split(':');
              r = await prismaService[model].findFirst({
                where: { id: Number(id) }
              });
              cache.set(`${type}:${r.id}`, JSON.stringify(r));
            }
            return r;
          })
        )
      ).filter((i) => i);
    } else {
      result = await cache
        .get(keys.key)
        .then((res) => JSON.parse(res))
        .catch(() => undefined);
      if (!result) {
        const [model, id] = keys.key.split(':');
        result = await prismaService[model].findFirst({
          where: { id: Number(id) }
        });
        cache.set(`${type}:${result.id}`, JSON.stringify(result));
      }
    }
  }
  if (!result) {
    result = await next();
    if (Array.isArray(result)) {
      for (const iterator of result) {
        cache.set(`${type}:${iterator.id}`, JSON.stringify(iterator));
      }
      await cache.set(
        key,
        JSON.stringify(result.map(({ id }) => `${type}:${id}`))
      );
    } else {
      cache.set(`${type}:${result.id}`, JSON.stringify(result));
      await cache.set(
        key,
        JSON.stringify({
          key: `${type}:${result.id}`
        })
      );
    }
  }
  return result;
};
