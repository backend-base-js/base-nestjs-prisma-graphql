import { Module } from '@nestjs/common';
import { PostAddressService } from './post-address.service';
import { PostAddressResolver } from './post-address.resolver';
import { PrismaModule } from '@/prisma/prisma.module';

@Module({
  imports: [PrismaModule],
  providers: [PostAddressResolver, PostAddressService],
  exports: [PostAddressService]
})
export class PostAddressModule {}
