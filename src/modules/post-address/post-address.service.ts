import { BaseService } from '@/utils/base/base.service';
import { PrismaService } from '@/prisma/prisma.service';
import { Injectable } from '@nestjs/common';
import { Prisma } from '@prisma/client';

@Injectable()
export class PostAddressService implements BaseService {
  constructor(private readonly prismaService: PrismaService) {}

  create(args: Prisma.PostAddressCreateArgs) {
    return this.prismaService.postAddress.create(args);
  }

  findUnique(args: Prisma.PostAddressFindUniqueArgs) {
    return this.prismaService.postAddress.findUnique(args);
  }

  findFirst(args: Prisma.PostAddressFindFirstArgs) {
    return this.prismaService.postAddress.findFirst(args);
  }

  findMany(args: Prisma.PostAddressFindManyArgs) {
    return this.prismaService.postAddress.findMany(args);
  }

  count(args: Prisma.PostAddressCountArgs) {
    return this.prismaService.postAddress.count(args);
  }

  update(args: Prisma.PostAddressUpdateArgs) {
    return this.prismaService.postAddress.update(args);
  }

  delete(args: Prisma.PostAddressDeleteArgs) {
    return this.prismaService.postAddress.delete(args);
  }
}
