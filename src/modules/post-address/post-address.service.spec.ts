import { Test, TestingModule } from '@nestjs/testing';
import { PostAddressService } from './post-address.service';

describe('PostAddressService', () => {
  let service: PostAddressService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [PostAddressService]
    }).compile();

    service = module.get<PostAddressService>(PostAddressService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
