import { PostAddressLoader } from '@/modules/common/dataloader/loaders/post-address.loader';
import {
  CreateOnePostAddressArgs,
  DeleteOnePostAddressArgs,
  FindManyPostAddressArgs,
  Post,
  PostAddress,
  UpdateOnePostAddressArgs,
  User
} from '@/prisma/graphql';
import {
  Args,
  Mutation,
  Parent,
  Query,
  ResolveField,
  Resolver
} from '@nestjs/graphql';
import * as DataLoader from 'dataloader';
import { Loader } from 'nestjs-dataloader';
import { PostAddressService } from './post-address.service';

@Resolver(() => PostAddress)
export class PostAddressResolver {
  constructor(private readonly postAddressService: PostAddressService) {}

  @Mutation(() => PostAddress, { name: 'create_post_address' })
  create(@Args() args: CreateOnePostAddressArgs) {
    return this.postAddressService.create(args);
  }

  @Query(() => [PostAddress], { name: 'all_post_address' })
  all(@Args() args: FindManyPostAddressArgs) {
    return this.postAddressService.findMany(args);
  }

  @Mutation(() => PostAddress, { name: 'update_post_address' })
  update(@Args() args: UpdateOnePostAddressArgs) {
    return this.postAddressService.update(args);
  }

  @Mutation(() => PostAddress, { name: 'delete_post_address' })
  delete(@Args() args: DeleteOnePostAddressArgs) {
    return this.postAddressService.delete(args);
  }

  @ResolveField(() => Post)
  posts(
    @Parent() parent: Post,
    @Loader(PostAddressLoader)
    postAddressLoader: DataLoader<User['id'], Post>
  ) {
    return postAddressLoader.load(parent.addressId);
  }
}
