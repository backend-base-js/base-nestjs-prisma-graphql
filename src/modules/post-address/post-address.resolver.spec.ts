import { Test, TestingModule } from '@nestjs/testing';
import { PostAddressResolver } from './post-address.resolver';
import { PostAddressService } from './post-address.service';

describe('PostAddressResolver', () => {
  let resolver: PostAddressResolver;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [PostAddressResolver, PostAddressService]
    }).compile();

    resolver = module.get<PostAddressResolver>(PostAddressResolver);
  });

  it('should be defined', () => {
    expect(resolver).toBeDefined();
  });
});
