import { PrismaModule } from '@/prisma/prisma.module';
import { Module } from '@nestjs/common';
import { FileResolver } from './file.resolver';
import { FileService } from './file.service';

@Module({
  imports: [PrismaModule],
  providers: [FileResolver, FileService],
  exports: [FileService]
})
export class FileModule {}
