import { Args, Query, Resolver } from '@nestjs/graphql';
import { CloudinaryService } from './cloudinary.service';
import { SignUrlReq } from './dtos/cloudinary.req';
import { SignUrlRes } from './dtos/cloudinary.res';

@Resolver()
export class CloudinaryResolver {
  constructor(private readonly cloudinaryService: CloudinaryService) {}

  @Query(() => SignUrlRes, { name: 'get_sign_url' })
  getSignUrl(@Args('args') args: SignUrlReq) {
    return this.cloudinaryService.signUrl({ ...args });
  }
}
