import { Field, InputType } from '@nestjs/graphql';

@InputType()
export class SignUrlReq {
  @Field(() => String)
  fileName: string;
}
