import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class SignUrlRes {
  @Field(() => String)
  url: string;

  @Field(() => String)
  publicId: string;
}
