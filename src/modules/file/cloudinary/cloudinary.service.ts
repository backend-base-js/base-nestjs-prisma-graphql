import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { v2 } from 'cloudinary';
import { format } from 'date-fns';
import { FileService } from '@/modules/file/file.service';
import * as slug from 'slug';
import { SignUrlRes } from './dtos/cloudinary.res';
import { ConfigService } from '@nestjs/config';
import { AppConfig } from '@/config/type';

@Injectable()
export class CloudinaryService {
  constructor(
    private readonly fileService: FileService,
    private readonly configService: ConfigService<AppConfig>
  ) {}

  /**
   *
   * @param args SignUrlReq
   * @returns SignUrlRes
   */
  signUrl(args: { fileName: string; folder?: string }): SignUrlRes {
    const folderName =
      args.folder || this.configService.get('FILE_FOLDER_ASSETS');
    const fileName = `${format(new Date(), 'yyyyMMddHHmmss')}-${slug(
      args.fileName
    )}`;

    const publicId = `${folderName}/${fileName}`;
    const timestamp = Math.round(new Date().getTime() / 1000);

    const signatureData = v2.utils.sign_request({
      timestamp,
      public_id: publicId
    });

    if (!signatureData || Object.values(signatureData).length <= 0)
      throw new HttpException(
        'Get sign url error.',
        HttpStatus.INTERNAL_SERVER_ERROR
      );

    const signUrlUpload = this.getUrlUpload({ ...signatureData });

    return {
      url: signUrlUpload,
      publicId
    };
  }

  getUrlUpload(params: {
    [key: string]: any;
    signature: string;
    api_key: string;
  }) {
    const { cloud_name, api_key, cloudinary_domain } = v2.config();
    return `${cloudinary_domain}${cloud_name}/upload?api_key=${api_key}&signature=${params.signature}&timestamp=${params.timestamp}&public_id=${params.public_id}`;
  }

  /**
   *
   * @param publicIds string[]
   * @param folder string | undefined
   * @returns File[]
   */
  async moveFiles(publicIds: string[], folder?: string) {
    const newFolder = folder || this.configService.get('FILE_FOLDER_ASSETS');
    if (!publicIds)
      throw new HttpException('Public id is required.', HttpStatus.BAD_REQUEST);

    const result = await Promise.all(
      publicIds.map(async (item) => {
        const splitName = item.split('/');
        const fileName = splitName[splitName.length - 1];
        const newPublicId = `${newFolder}/${fileName}`;

        const newCloudinaryFile = await v2.uploader.rename(item, newPublicId);

        // check insert or update to database
        const fileValid = await this.fileService.findFirst({
          where: { publicId: { equals: newPublicId } }
        });

        if (fileValid) {
          return await this.fileService.update({
            where: {
              id: fileValid.id
            },
            data: {
              publicId: newCloudinaryFile.public_id,
              url: newCloudinaryFile.url
            }
          });
        }

        // insert file
        return await this.fileService.create({
          data: {
            url: newCloudinaryFile.url,
            publicId: newCloudinaryFile.public_id,
            metadata: {
              format: newCloudinaryFile.format,
              resource_type: newCloudinaryFile.resource_type,
              bytes: newCloudinaryFile.bytes,
              height: newCloudinaryFile.height,
              width: newCloudinaryFile.width
            }
          }
        });
      })
    );

    return result;
  }

  /**
   *
   * @param publicIds string[]
   * @returns File[]
   */
  async delete(publicIds: string[]) {
    const result = await Promise.all(
      publicIds.map(async (item) => {
        // call cloudinary delete and database
        const fileCloudinaryDelete = await v2.uploader.destroy(item);
        if (fileCloudinaryDelete.result === 'ok') {
          return await this.fileService.delete({
            where: { publicId: item }
          });
        }
      })
    );
    return result;
  }

  /**
   *
   * @param urls string[]
   * @param folder string | undefined
   * @returns File[]
   */
  async uploadRemoteFiles(urls: string[], folder: string) {
    const folderName = folder || this.configService.get('FILE_FOLDER_ASSETS');
    const timestamp = Math.round(new Date().getTime() / 1000);

    const results = await Promise.all(
      urls.map(async (item) => {
        const imageUpload = await v2.uploader.upload(item, {
          timestamp,
          folder: folderName
        });

        // insert file
        return await this.fileService.create({
          data: {
            url: imageUpload.url,
            publicId: imageUpload.public_id,
            metadata: {
              format: imageUpload.format,
              resource_type: imageUpload.resource_type,
              bytes: imageUpload.bytes,
              height: imageUpload.height,
              width: imageUpload.width
            }
          }
        });
      })
    );

    return results;
  }
}
