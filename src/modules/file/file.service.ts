import { BaseService } from '@/utils/base/base.service';
import { PrismaService } from '@/prisma/prisma.service';
import { Injectable } from '@nestjs/common';
import { Prisma } from '@prisma/client';

@Injectable()
export class FileService implements BaseService {
  constructor(private readonly prismaService: PrismaService) {}

  create(args: Prisma.FileCreateArgs) {
    return this.prismaService.file.create(args);
  }

  findUnique(args: Prisma.FileFindUniqueArgs) {
    return this.prismaService.file.findUnique(args);
  }

  findFirst(args: Prisma.FileFindFirstArgs) {
    return this.prismaService.file.findFirst(args);
  }

  findMany(args: Prisma.FileFindManyArgs) {
    return this.prismaService.file.findMany(args);
  }

  count(args: Prisma.FileCountArgs) {
    return this.prismaService.file.count(args);
  }

  update(args: Prisma.FileUpdateManyArgs) {
    return this.prismaService.file.updateMany(args);
  }

  delete(args: Prisma.FileDeleteArgs) {
    return this.prismaService.file.delete(args);
  }
}
