import { HttpService } from '@nestjs/axios';
import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { readJsonFile } from '@/utils/helpers';
import * as firebase from 'firebase-admin';
import { ServiceAccount } from 'firebase-admin';
import { Auth } from 'firebase-admin/lib/auth/auth';
import { AppConfig } from '@/config/type';

@Injectable()
export class FirebaseService {
  private firebaseApp: firebase.app.App;
  private firebaseAuth: Auth;
  constructor(
    private readonly configService: ConfigService<AppConfig>,
    private readonly httpService: HttpService
  ) {
    const credentialPath = this.configService.get<string>(
      'FIREBASE_CREDENTIAL'
    );
    const credential = readJsonFile<ServiceAccount>(credentialPath);
    this.firebaseApp = firebase.initializeApp({
      credential: firebase.credential.cert(credential)
    });
    this.firebaseAuth = this.firebaseApp.auth();
  }

  async verifyIdToken(idToken: string, checkRevoked?: boolean) {
    return this.firebaseAuth.verifyIdToken(idToken, checkRevoked);
  }

  async signInWithPasswordTest() {
    const data = JSON.parse(this.configService.get('FIREBASE_LOGIN_DATA_TEST'));
    return this.httpService.axiosRef
      .post<{ idToken: string } | undefined>(
        'https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword',
        {
          ...data,
          returnSecureToken: true
        },
        {
          params: {
            key: this.configService.get<string>('FIREBASE_WEB_API_KEY')
          }
        }
      )
      .then((res) => {
        return res.data;
      });
  }
}
