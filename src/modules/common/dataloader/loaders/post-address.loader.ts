import * as DataLoader from 'dataloader';
import { Injectable } from '@nestjs/common';
import { NestDataLoader } from 'nestjs-dataloader';
import { PostAddress } from '@prisma/client';
import { PostAddressService } from '@/modules/post-address/post-address.service';

@Injectable()
export class PostAddressLoader implements NestDataLoader<string, PostAddress> {
  constructor(private readonly postAddressService: PostAddressService) {}

  generateDataLoader(): DataLoader<string, PostAddress> {
    return new DataLoader<string, PostAddress>((keys) => {
      return Promise.all(
        keys.map((key) => {
          return this.postAddressService.findUnique({
            where: {
              id: key
            }
          });
        })
      );
    });
  }
}
