import { AddressService } from '@/modules/address/address.service';
import { Injectable } from '@nestjs/common';
import { Address } from '@prisma/client';
import * as DataLoader from 'dataloader';
import { NestDataLoader } from 'nestjs-dataloader';

@Injectable()
export class AddressLoader implements NestDataLoader<string, Address> {
  constructor(private readonly addressService: AddressService) {}

  generateDataLoader(): DataLoader<string, Address> {
    return new DataLoader<string, Address>((keys) => {
      return Promise.all(
        keys.map((key) => {
          return this.addressService.findUnique({
            where: {
              id: key
            }
          });
        })
      );
    });
  }
}

@Injectable()
export class AddressByParentLoader
  implements NestDataLoader<string, Address[]>
{
  constructor(private readonly addressService: AddressService) {}

  generateDataLoader(): DataLoader<string, Address[]> {
    return new DataLoader<string, Address[]>((keys) => {
      return Promise.all(
        keys.map((key) => {
          return this.addressService
            .findUnique({
              where: {
                id: key
              }
            })
            .children();
        })
      );
    });
  }
}
