import { UserService } from '@/modules/user/user.service';
import { Injectable } from '@nestjs/common';
import { Post } from '@prisma/client';
import * as DataLoader from 'dataloader';
import { NestDataLoader } from 'nestjs-dataloader';

@Injectable()
export class PostByAuthorLoader implements NestDataLoader<string, Post[]> {
  constructor(private readonly userService: UserService) {}

  generateDataLoader(): DataLoader<string, Post[]> {
    return new DataLoader<string, Post[]>((keys) => {
      return Promise.all(
        keys.map((key) => {
          return this.userService
            .findUnique({
              where: {
                id: key
              }
            })
            .posts();
        })
      );
    });
  }
}
