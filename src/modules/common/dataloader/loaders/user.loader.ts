import * as DataLoader from 'dataloader';
import { Injectable } from '@nestjs/common';
import { NestDataLoader } from 'nestjs-dataloader';
import { UserService } from '@/modules/user/user.service';
import { User } from '@prisma/client';

@Injectable()
export class UserLoader implements NestDataLoader<string, User> {
  constructor(private readonly userService: UserService) {}

  generateDataLoader(): DataLoader<string, User> {
    return new DataLoader<string, User>((keys) => {
      return Promise.all(
        keys.map((key) => {
          return this.userService.findUnique({
            where: {
              id: key
            }
          });
        })
      );
    });
  }
}
