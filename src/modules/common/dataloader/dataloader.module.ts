import { AddressModule } from '@/modules/address/address.module';
import { PostAddressModule } from '@/modules/post-address/post-address.module';
import { UserModule } from '@/modules/user/user.module';
import { PrismaModule } from '@/prisma/prisma.module';
import { Module } from '@nestjs/common';
import { APP_INTERCEPTOR } from '@nestjs/core';
import { DataLoaderInterceptor } from 'nestjs-dataloader';
import { AddressByParentLoader, AddressLoader } from './loaders/address.loader';
import { PostAddressLoader } from './loaders/post-address.loader';
import { PostByAuthorLoader } from './loaders/post.loader';
import { UserLoader } from './loaders/user.loader';

@Module({
  imports: [PrismaModule, UserModule, PostAddressModule, AddressModule],
  providers: [
    UserLoader,
    PostByAuthorLoader,
    PostAddressLoader,
    AddressLoader,
    AddressByParentLoader,
    {
      provide: APP_INTERCEPTOR,
      useClass: DataLoaderInterceptor
    }
  ]
})
export class DataloaderModule {}
