import {
  DeleteOneSessionArgs,
  FindManySessionArgs,
  Session
} from '@/prisma/graphql';
import { TakeLimit } from '@/utils/pipes/take-limit.decorator';
import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import { SessionService } from './session.service';

@Resolver(() => Session)
export class SessionResolver {
  constructor(private readonly sessionService: SessionService) {}

  @Query(() => [Session], { name: 'all_session' })
  all(@Args(new TakeLimit()) args: FindManySessionArgs) {
    return this.sessionService.findMany(args);
  }

  @Mutation(() => Session, { name: 'delete_session' })
  delete(@Args() args: DeleteOneSessionArgs) {
    return this.sessionService.delete(args);
  }
}
