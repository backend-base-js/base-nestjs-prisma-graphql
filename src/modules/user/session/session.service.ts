import { PrismaService } from '@/prisma/prisma.service';
import { Injectable } from '@nestjs/common';
import { Prisma } from '@prisma/client';

@Injectable()
export class SessionService {
  constructor(private readonly prismaService: PrismaService) {}

  create(args: Prisma.SessionCreateArgs) {
    return this.prismaService.session.create(args);
  }

  findUnique(args: Prisma.SessionFindUniqueArgs) {
    return this.prismaService.session.findUnique(args);
  }

  findFirst(args: Prisma.SessionFindFirstArgs) {
    return this.prismaService.session.findFirst(args);
  }

  findMany(args: Prisma.SessionFindManyArgs) {
    return this.prismaService.session.findMany(args);
  }

  count(args: Prisma.SessionCountArgs) {
    return this.prismaService.session.count(args);
  }

  update(args: Prisma.SessionUpdateArgs) {
    return this.prismaService.session.update(args);
  }

  delete(args: Prisma.SessionDeleteArgs) {
    return this.prismaService.session.delete(args);
  }
}
