import {
  ExecutionContext,
  Injectable,
  UnauthorizedException
} from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { GqlExecutionContext } from '@nestjs/graphql';
import { AuthGuard } from '@nestjs/passport';
import { AuthJwtOptions, AUTH_JWT_OPTIONS_KEY } from './jwt.decorator';

@Injectable()
export class AuthJwtGuard extends AuthGuard('jwt') {
  private options: undefined | AuthJwtOptions;

  constructor(private reflector: Reflector) {
    super();
  }

  getRequest(context: ExecutionContext) {
    const ctx = GqlExecutionContext.create(context);
    return ctx.getContext().req;
  }

  canActivate(context) {
    // Add your custom authentication logic here
    // for example, call super.logIn(request) to establish a session.
    // const ctx = GqlExecutionContext.create(context);
    this.options = this.reflector.getAllAndOverride<AuthJwtOptions>(
      AUTH_JWT_OPTIONS_KEY,
      [context.getHandler(), context.getClass()]
    );

    if (this.options?.roles) {
      return super.canActivate(context);
    }
    return true;
  }

  handleRequest(err, user) {
    // You can throw an exception based on either "info" or "err" arguments
    if (
      err ||
      !user ||
      (this.options?.roles?.length && !this.options?.roles.includes(user.type))
    ) {
      throw err || new UnauthorizedException();
    }
    return user;
  }
}
