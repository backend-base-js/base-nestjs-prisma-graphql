import {
  applyDecorators,
  createParamDecorator,
  ExecutionContext,
  SetMetadata,
  UnauthorizedException,
  UseGuards
} from '@nestjs/common';
import { AuthJwtGuard } from './jwt.guard';
import { JwtStrategy } from './jwt.strategy';
export type Payload = {
  id: string;
  type: 'user';
};

export type AuthJwtOptions = {
  roles?: string[];
};

export const AUTH_JWT_OPTIONS_KEY = 'AUTH_JWT_OPTIONS';

export const AuthJwt = (options: AuthJwtOptions = { roles: [] }) => {
  return applyDecorators(
    SetMetadata(AUTH_JWT_OPTIONS_KEY, options),
    UseGuards(JwtStrategy, AuthJwtGuard)
  );
};

export const ReqAuth = createParamDecorator<boolean, ExecutionContext, Payload>(
  (exception = false, ctx: ExecutionContext) => {
    const request = ctx.switchToHttp().getRequest();
    if (exception && !request.user?.id) {
      throw new UnauthorizedException();
    }
    return request.user as Payload;
  }
);
