import { AppConfig } from '@/config/type';
import { PrismaModule } from '@/prisma/prisma.module';
import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { JwtModule } from '@nestjs/jwt';
import { FirebaseModule } from '../common/firebase/firebase.module';
import { SessionModule } from './session/session.module';
import { UserResolver } from './user.resolver';
import { UserService } from './user.service';

@Module({
  imports: [
    PrismaModule,
    FirebaseModule,
    UserModule,
    JwtModule.registerAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (configService: ConfigService<AppConfig>) => {
        return {
          secret: configService.get('JWT_SECRET_KEY'),
          signOptions: {
            expiresIn: configService.get<number>('JWT_EXPIRES_IN')
          }
        };
      }
    }),
    SessionModule
  ],
  providers: [UserResolver, UserService],
  exports: [UserService]
})
export class UserModule {}
