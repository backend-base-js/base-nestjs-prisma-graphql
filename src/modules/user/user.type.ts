import { User } from '@/prisma/graphql';
import { ObjectType, OmitType } from '@nestjs/graphql';

import { ArgsType, Field } from '@nestjs/graphql';

@ObjectType()
export class UserOnly extends OmitType(User, ['messages']) {}

@ArgsType()
export class LoginArgs {
  @Field()
  idToken: string;
}

@ObjectType()
export class LoginResult {
  @Field()
  accessToken: string;

  @Field()
  expires: Date;

  @Field(() => User, { nullable: true })
  profile?: User;
}
