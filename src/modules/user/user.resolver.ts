import { PostByAuthorLoader } from '@/modules/common/dataloader/loaders/post.loader';
import { FindManyUserArgs, Post, User } from '@/prisma/graphql';
import { TakeLimit } from '@/utils/pipes/take-limit.decorator';
import {
  Args,
  Mutation,
  Parent,
  Query,
  ResolveField,
  Resolver
} from '@nestjs/graphql';
import * as DataLoader from 'dataloader';
import { Loader } from 'nestjs-dataloader';
import { UserService } from './user.service';
import { LoginArgs, LoginResult, UserOnly } from './user.type';

@Resolver(() => UserOnly)
export class UserResolver {
  constructor(private readonly userService: UserService) {}

  @Query(() => [UserOnly], { name: 'all_user' })
  all(@Args(new TakeLimit()) args: FindManyUserArgs) {
    return this.userService.findMany(args);
  }

  @ResolveField(() => [Post])
  posts(
    @Parent() parent: User,
    @Loader(PostByAuthorLoader)
    postByAuthorLoader: DataLoader<User['id'], Post[]>
  ) {
    return postByAuthorLoader.load(parent.id);
  }

  @Mutation(() => LoginResult, { name: 'auth_login_test' })
  async getTestToken(): Promise<LoginResult> {
    return this.userService.loginTest();
  }

  @Mutation(() => LoginResult, { name: 'auth_login' })
  async login(@Args() args: LoginArgs): Promise<LoginResult> {
    return this.userService.login(args);
  }
}
