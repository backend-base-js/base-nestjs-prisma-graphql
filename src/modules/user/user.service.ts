import { AppConfig } from '@/config/type';
import { PrismaService } from '@/prisma/prisma.service';
import { BaseService } from '@/utils/base/base.service';
import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import { Prisma } from '@prisma/client';
import { add } from 'date-fns';
import { FirebaseService } from '../common/firebase/firebase.service';
import { LoginArgs, LoginResult } from './user.type';

@Injectable()
export class UserService implements BaseService {
  constructor(
    private readonly prismaService: PrismaService,
    private readonly jwtService: JwtService,
    private readonly firebaseService: FirebaseService,
    private readonly configService: ConfigService<AppConfig>
  ) {}

  create(args: Prisma.UserCreateArgs) {
    return this.prismaService.user.create(args);
  }

  findUnique(args: Prisma.UserFindUniqueArgs) {
    return this.prismaService.user.findUnique(args);
  }

  findFirst(args: Prisma.UserFindFirstArgs) {
    return this.prismaService.user.findFirst(args);
  }

  findMany(args: Prisma.UserFindManyArgs) {
    return this.prismaService.user.findMany(args);
  }

  count(args: Prisma.UserCountArgs) {
    return this.prismaService.user.count(args);
  }

  update(args: Prisma.UserUpdateArgs) {
    return this.prismaService.user.update(args);
  }

  delete(args: Prisma.UserDeleteArgs) {
    return this.prismaService.user.delete(args);
  }

  async login(args: LoginArgs): Promise<LoginResult> {
    const decodedIdToken = await this.firebaseService.verifyIdToken(
      args.idToken
    );

    let user = await this.findFirst({
      where: {
        id: {
          equals: decodedIdToken.uid
        }
      }
    });
    if (!user) {
      user = await this.create({
        data: {
          name: decodedIdToken.name || decodedIdToken.email,
          email: decodedIdToken.email,
          emailVerified: !!decodedIdToken.email_verified,
          signInProvider: decodedIdToken.firebase.sign_in_provider
        }
      });
    }
    const payload = {
      id: user.id
    };
    const expiresIn = this.configService.get<number>('JWT_EXPIRES_IN');
    return {
      accessToken: await this.jwtService.sign(payload),
      expires: add(new Date(), {
        seconds: expiresIn
      }),
      profile: user
    };
  }

  async loginTest(): Promise<LoginResult> {
    const result = await this.firebaseService.signInWithPasswordTest();
    return this.login({ idToken: result?.idToken });
  }
}
