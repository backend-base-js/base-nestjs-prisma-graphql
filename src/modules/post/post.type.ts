import { Post } from '@/prisma/graphql';
import { Field, ObjectType } from '@nestjs/graphql';
import { UserOnly } from '../user/user.type';

@ObjectType()
export class PostWithAuthor extends Post {
  @Field(() => UserOnly, { nullable: false })
  author?: InstanceType<typeof UserOnly>;
}
