import { PrismaModule } from '@/prisma/prisma.module';
import { Module } from '@nestjs/common';
import { NotificationModule } from '../notification/notification.module';
import { UserModule } from '../user/user.module';
import { PostResolver } from './post.resolver';
import { PostService } from './post.service';

@Module({
  imports: [PrismaModule, UserModule, NotificationModule],
  providers: [PostResolver, PostService],
  exports: [PostService]
})
export class PostModule {}
