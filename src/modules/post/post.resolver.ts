import { PostAddressLoader } from '@/modules/common/dataloader/loaders/post-address.loader';
import { UserLoader } from '@/modules/common/dataloader/loaders/user.loader';
import {
  CreateOnePostArgs,
  DeleteOnePostArgs,
  FindManyPostArgs,
  Post,
  PostAddress,
  UpdateOnePostArgs,
  User
} from '@/prisma/graphql';
import { TakeLimit } from '@/utils/pipes/take-limit.decorator';
import {
  Args,
  Mutation,
  Parent,
  Query,
  ResolveField,
  Resolver
} from '@nestjs/graphql';
import * as DataLoader from 'dataloader';
import { Loader } from 'nestjs-dataloader';
import { PostService } from './post.service';

@Resolver(() => Post)
export class PostResolver {
  constructor(private readonly postService: PostService) {}

  @Mutation(() => Post, { name: 'create_post' })
  create(@Args() args: CreateOnePostArgs) {
    return this.postService.create(args);
  }

  @Query(() => [Post], { name: 'all_post' })
  all(@Args(new TakeLimit(100)) args: FindManyPostArgs) {
    return this.postService.findMany(args);
  }

  @Mutation(() => Post, { name: 'update_post' })
  update(@Args() args: UpdateOnePostArgs) {
    return this.postService.update(args);
  }

  @Mutation(() => Post, { name: 'delete_post' })
  delete(@Args() args: DeleteOnePostArgs) {
    return this.postService.delete(args);
  }

  @ResolveField(() => User)
  author(
    @Parent() parent: Post,
    @Loader(UserLoader)
    userLoader: DataLoader<User['id'], User>
  ) {
    return userLoader.load(parent.authorId);
  }

  @ResolveField(() => PostAddress)
  address(
    @Parent() parent: Post,
    @Loader(PostAddressLoader)
    postAddressLoader: DataLoader<PostAddress['id'], PostAddress>
  ) {
    return postAddressLoader.load(parent.addressId);
  }
}
