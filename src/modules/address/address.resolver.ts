import {
  AddressByParentLoader,
  AddressLoader
} from '@/modules/common/dataloader/loaders/address.loader';
import { Address, FindManyAddressArgs } from '@/prisma/graphql';
import { TakeLimit } from '@/utils/pipes/take-limit.decorator';
import { Args, Parent, Query, ResolveField, Resolver } from '@nestjs/graphql';
import * as DataLoader from 'dataloader';
import { Loader } from 'nestjs-dataloader';
import { AddressService } from './address.service';

@Resolver(() => Address)
export class AddressResolver {
  constructor(private readonly addressService: AddressService) {}

  @Query(() => [Address], { name: 'all_address' })
  all(@Args(new TakeLimit()) args: FindManyAddressArgs) {
    return this.addressService.findMany(args);
  }

  @Query(() => Number, { name: 'count_address' })
  count(@Args() args: FindManyAddressArgs) {
    return this.addressService.count(args);
  }

  @ResolveField(() => Address)
  parent(
    @Parent() parent: Address,
    @Loader(AddressLoader)
    userLoader: DataLoader<Address['id'], Address>
  ) {
    return userLoader.load(parent.parentId);
  }

  @ResolveField(() => Address)
  children(
    @Parent() parent: Address,
    @Loader(AddressByParentLoader)
    addressByParentLoader: DataLoader<Address['id'], Address[]>
  ) {
    return addressByParentLoader.load(parent.id);
  }
}
