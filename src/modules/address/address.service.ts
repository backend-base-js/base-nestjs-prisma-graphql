import { PrismaService } from '@/prisma/prisma.service';
import { BaseService } from '@/utils/base/base.service';
import { Injectable } from '@nestjs/common';
import { Prisma } from '@prisma/client';

@Injectable()
export class AddressService implements BaseService {
  constructor(private readonly prismaService: PrismaService) {}

  async create(args: Prisma.AddressCreateArgs) {
    return this.prismaService.address.create(args);
  }

  findUnique(args: Prisma.AddressFindUniqueArgs) {
    return this.prismaService.address.findUnique(args);
  }

  findFirst(args: Prisma.AddressFindFirstArgs) {
    return this.prismaService.address.findFirst(args);
  }

  findMany(args: Prisma.AddressFindManyArgs) {
    return this.prismaService.address.findMany(args);
  }

  count(args: Prisma.AddressCountArgs) {
    return this.prismaService.address.count(args);
  }

  update(args: Prisma.AddressUpdateArgs) {
    return this.prismaService.address.update(args);
  }

  delete(args: Prisma.AddressDeleteArgs) {
    return this.prismaService.address.delete(args);
  }
}
