import { PrismaModule } from '@/prisma/prisma.module';
import { Module } from '@nestjs/common';
import { AddressResolver } from './address.resolver';
import { AddressService } from './address.service';

@Module({
  imports: [PrismaModule],
  providers: [AddressResolver, AddressService],
  exports: [AddressService]
})
export class AddressModule {}
