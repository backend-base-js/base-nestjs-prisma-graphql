import { PrismaModule } from '@/prisma/prisma.module';
import { BullModule } from '@nestjs/bull';
import { Module } from '@nestjs/common';
import { NotificationResolver } from './notification.resolver';
import {
  NotificationConsumer,
  NotificationService
} from './notification.service';

@Module({
  imports: [
    PrismaModule,
    BullModule.registerQueue({
      name: 'notification'
    })
  ],
  providers: [NotificationResolver, NotificationService, NotificationConsumer],
  exports: [NotificationService]
})
export class NotificationModule {}
