import { PrismaService } from '@/prisma/prisma.service';
import { BaseService } from '@/utils/base/base.service';
import {
  InjectQueue,
  OnQueueProgress,
  OnQueueWaiting,
  Processor
} from '@nestjs/bull';
import { Injectable } from '@nestjs/common';
import { Prisma } from '@prisma/client';
import { Job, Queue } from 'bull';

@Injectable()
export class NotificationService implements BaseService {
  constructor(
    private readonly prismaService: PrismaService,
    @InjectQueue('notification') private readonly notificationQueue: Queue
  ) {}

  create(args: Prisma.NotificationCreateArgs) {
    return this.prismaService.notification.create(args);
  }

  findUnique(args: Prisma.NotificationFindUniqueArgs) {
    return this.prismaService.notification.findUnique(args);
  }

  findFirst(args: Prisma.NotificationFindFirstArgs) {
    return this.prismaService.notification.findFirst(args);
  }

  findMany(args: Prisma.NotificationFindManyArgs) {
    return this.prismaService.notification.findMany(args);
  }

  count(args: Prisma.NotificationCountArgs) {
    return this.prismaService.notification.count(args);
  }

  update(args: Prisma.NotificationUpdateArgs) {
    return this.prismaService.notification.update(args);
  }

  delete(args: Prisma.NotificationDeleteArgs) {
    return this.prismaService.notification.delete(args);
  }

  sendNotification(data) {
    this.notificationQueue.add(data);
  }
}

@Processor('notification')
export class NotificationConsumer {
  constructor(private readonly notificationService: NotificationService) {}

  @OnQueueWaiting()
  onQueueWaiting(job: Job) {
    console.log(
      `Waiting job ${job.id} of type ${job.name} with data ${job.data}...`
    );
  }

  @OnQueueProgress()
  onQueueProgress(job: Job) {
    console.log(
      `Processing job ${job.id} of type ${job.name} with data ${job.data}...`
    );
  }
}
