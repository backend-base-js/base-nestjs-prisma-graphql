import { FindManyNotificationArgs, Notification } from '@/prisma/graphql';
import { TakeLimit } from '@/utils/pipes/take-limit.decorator';
import { Args, Query, Resolver } from '@nestjs/graphql';
import { NotificationService } from './notification.service';

@Resolver(() => Notification)
export class NotificationResolver {
  constructor(private readonly notificationService: NotificationService) {}

  @Query(() => [Notification], { name: 'all_notification' })
  all(@Args(new TakeLimit()) args: FindManyNotificationArgs) {
    return this.notificationService.findMany(args);
  }
}
