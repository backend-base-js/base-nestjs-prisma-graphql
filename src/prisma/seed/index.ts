import { faker } from '@faker-js/faker';
import { AddressType, PrismaClient } from '@prisma/client';
import { csvToObj } from 'csv-to-js-parser';
import * as fs from 'fs';
import * as path from 'path';

const prisma = new PrismaClient();

const arr = (length: number) => [...Array.from(new Array(length))];

const random = (arr: any) => arr[Math.floor(Math.random() * arr.length)];

async function main() {
  const data = fs
    .readFileSync(path.resolve(__dirname, './files/address.csv'))
    .toString();
  const addresses = csvToObj(data, ';');
  console.log('importing address');
  for (const iterator of addresses) {
    console.log(iterator);
    if (iterator.ward_code) {
      await prisma.address.create({
        data: {
          name: iterator.ward_name,
          code: iterator.ward_code,
          type: AddressType.ward,
          ...(iterator.district_code
            ? {
                parent: {
                  connectOrCreate: {
                    where: {
                      code: iterator.district_code
                    },
                    create: {
                      name: iterator.district_name,
                      code: iterator.district_code,
                      type: AddressType.district,
                      ...(iterator.city_code
                        ? {
                            parent: {
                              connectOrCreate: {
                                where: {
                                  code: iterator.city_code
                                },
                                create: {
                                  name: iterator[Object.keys(iterator)[0]],
                                  code: iterator.city_code,
                                  type: AddressType.city
                                }
                              }
                            }
                          }
                        : {})
                    }
                  }
                }
              }
            : {})
        }
      });
    }
  }

  console.log('creating users');
  const users = await Promise.all(
    arr(10).map(() =>
      prisma.user.create({
        data: {
          name: faker.name.fullName(),
          email: faker.internet.email(undefined, undefined, 'google.com'),
          signInProvider: 'google.com'
        }
      })
    )
  ).catch(() => prisma.user.findMany({ take: 10 }));

  console.log('creating category');
  const categories = await Promise.all(
    arr(10).map(() =>
      prisma.category.create({
        data: {
          name: faker.name.jobArea()
        }
      })
    )
  ).catch(() => prisma.category.findMany({ take: 10 }));

  console.log('creating posts');
  const postAddresses = await Promise.all(
    arr(50).map(() =>
      prisma.postAddress.create({
        data: {
          detail: faker.address.streetAddress()
        }
      })
    )
  ).catch(() => prisma.postAddress.findMany({ take: 10 }));
  const posts = await Promise.all(
    arr(50).map((_v, i) =>
      prisma.post.create({
        data: {
          content: faker.lorem.paragraph(),
          categoryId: random(categories).id,
          authorId: random(users).id,
          addressId: postAddresses[i].id
        }
      })
    )
  ).catch(() => prisma.post.findMany({ take: 50 }));
  return {
    user: users.length,
    category: categories.length,
    post: posts.length
  };
}
main()
  .then(async () => {
    await prisma.$disconnect();
  })
  .catch(async (e) => {
    console.error(e);
    await prisma.$disconnect();
    process.exit(1);
  });
