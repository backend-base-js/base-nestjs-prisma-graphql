-- CreateEnum

CREATE TYPE "UserStatus" AS ENUM ('active', 'inactive');

-- CreateEnum

CREATE TYPE "NotificationType" AS ENUM ('new_post', 'new_message');

-- CreateEnum

CREATE TYPE "AddressType" AS ENUM ('city', 'district', 'ward');

-- CreateTable

CREATE TABLE
    "User" (
        "id" TEXT NOT NULL,
        "avatarId" TEXT,
        "name" TEXT NOT NULL,
        "email" TEXT,
        "emailVerified" BOOLEAN NOT NULL DEFAULT true,
        "phone" TEXT,
        "signInProvider" TEXT NOT NULL,
        "status" "UserStatus" NOT NULL DEFAULT 'active',
        "addressDetail" TEXT,
        "addressId" TEXT,
        "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
        "updatedAt" TIMESTAMP(3) NOT NULL,
        "deletedAt" TIMESTAMP(3),
        CONSTRAINT "User_pkey" PRIMARY KEY ("id")
    );

-- CreateTable

CREATE TABLE
    "Post" (
        "id" TEXT NOT NULL,
        "categoryId" TEXT NOT NULL,
        "content" TEXT NOT NULL,
        "addressId" TEXT NOT NULL,
        "authorId" TEXT NOT NULL,
        "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
        "updatedAt" TIMESTAMP(3) NOT NULL,
        "deletedAt" TIMESTAMP(3),
        CONSTRAINT "Post_pkey" PRIMARY KEY ("id")
    );

-- CreateTable

CREATE TABLE
    "PostAddress" (
        "id" TEXT NOT NULL,
        "addressId" TEXT,
        "detail" TEXT NOT NULL,
        CONSTRAINT "PostAddress_pkey" PRIMARY KEY ("id")
    );

-- CreateTable

CREATE TABLE
    "Category" (
        "id" TEXT NOT NULL,
        "name" TEXT NOT NULL,
        "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
        "updatedAt" TIMESTAMP(3) NOT NULL,
        "deletedAt" TIMESTAMP(3),
        CONSTRAINT "Category_pkey" PRIMARY KEY ("id")
    );

-- CreateTable

CREATE TABLE
    "Notification" (
        "id" TEXT NOT NULL,
        "type" "NotificationType" NOT NULL,
        "content" TEXT NOT NULL,
        "userId" TEXT NOT NULL,
        "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
        "updatedAt" TIMESTAMP(3) NOT NULL,
        "deletedAt" TIMESTAMP(3),
        CONSTRAINT "Notification_pkey" PRIMARY KEY ("id")
    );

-- CreateTable

CREATE TABLE
    "Conversation" (
        "id" TEXT NOT NULL,
        "name" TEXT,
        "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
        "updatedAt" TIMESTAMP(3) NOT NULL,
        "deletedAt" TIMESTAMP(3),
        CONSTRAINT "Conversation_pkey" PRIMARY KEY ("id")
    );

-- CreateTable

CREATE TABLE
    "Message" (
        "id" TEXT NOT NULL,
        "content" TEXT NOT NULL,
        "userId" TEXT NOT NULL,
        "conversationId" TEXT NOT NULL,
        "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
        "updatedAt" TIMESTAMP(3) NOT NULL,
        "deletedAt" TIMESTAMP(3),
        CONSTRAINT "Message_pkey" PRIMARY KEY ("id")
    );

-- CreateTable

CREATE TABLE
    "File" (
        "id" TEXT NOT NULL,
        "url" TEXT NOT NULL,
        "publicId" TEXT NOT NULL,
        "metadata" JSONB NOT NULL,
        "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
        "updatedAt" TIMESTAMP(3) NOT NULL,
        "deletedAt" TIMESTAMP(3),
        CONSTRAINT "File_pkey" PRIMARY KEY ("id")
    );

-- CreateTable

CREATE TABLE
    "Address" (
        "id" TEXT NOT NULL,
        "code" TEXT NOT NULL,
        "name" TEXT NOT NULL,
        "type" "AddressType" NOT NULL,
        "parentId" TEXT,
        "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
        "updatedAt" TIMESTAMP(3) NOT NULL,
        "deletedAt" TIMESTAMP(3),
        CONSTRAINT "Address_pkey" PRIMARY KEY ("id")
    );

-- CreateTable

CREATE TABLE
    "Setting" (
        "key" TEXT NOT NULL,
        "value" JSONB NOT NULL,
        CONSTRAINT "Setting_pkey" PRIMARY KEY ("key")
    );

-- CreateTable

CREATE TABLE
    "_CategoryToUser" (
        "A" TEXT NOT NULL,
        "B" TEXT NOT NULL
    );

-- CreateTable

CREATE TABLE
    "_ConversationToUser" (
        "A" TEXT NOT NULL,
        "B" TEXT NOT NULL
    );

-- CreateTable

CREATE TABLE
    "_FileToMessage" (
        "A" TEXT NOT NULL,
        "B" TEXT NOT NULL
    );

-- CreateIndex

CREATE UNIQUE INDEX "User_email_key" ON "User"("email");

-- CreateIndex

CREATE UNIQUE INDEX "User_phone_key" ON "User"("phone");

-- CreateIndex

CREATE UNIQUE INDEX "Post_addressId_key" ON "Post"("addressId");

-- CreateIndex

CREATE UNIQUE INDEX "Category_name_key" ON "Category"("name");

-- CreateIndex

CREATE UNIQUE INDEX "File_publicId_key" ON "File"("publicId");

-- CreateIndex

CREATE UNIQUE INDEX "Address_code_key" ON "Address"("code");

-- CreateIndex

CREATE UNIQUE INDEX "_CategoryToUser_AB_unique" ON "_CategoryToUser"("A", "B");

-- CreateIndex

CREATE INDEX "_CategoryToUser_B_index" ON "_CategoryToUser"("B");

-- CreateIndex

CREATE UNIQUE INDEX "_ConversationToUser_AB_unique" ON "_ConversationToUser"("A", "B");

-- CreateIndex

CREATE INDEX
    "_ConversationToUser_B_index" ON "_ConversationToUser"("B");

-- CreateIndex

CREATE UNIQUE INDEX "_FileToMessage_AB_unique" ON "_FileToMessage"("A", "B");

-- CreateIndex

CREATE INDEX "_FileToMessage_B_index" ON "_FileToMessage"("B");

-- AddForeignKey

ALTER TABLE "User"
ADD
    CONSTRAINT "User_avatarId_fkey" FOREIGN KEY ("avatarId") REFERENCES "File"("id") ON DELETE
SET NULL ON UPDATE CASCADE;

-- AddForeignKey

ALTER TABLE "User"
ADD
    CONSTRAINT "User_addressId_fkey" FOREIGN KEY ("addressId") REFERENCES "Address"("id") ON DELETE
SET NULL ON UPDATE CASCADE;

-- AddForeignKey

ALTER TABLE "Post"
ADD
    CONSTRAINT "Post_categoryId_fkey" FOREIGN KEY ("categoryId") REFERENCES "Category"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey

ALTER TABLE "Post"
ADD
    CONSTRAINT "Post_addressId_fkey" FOREIGN KEY ("addressId") REFERENCES "PostAddress"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey

ALTER TABLE "Post"
ADD
    CONSTRAINT "Post_authorId_fkey" FOREIGN KEY ("authorId") REFERENCES "User"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey

ALTER TABLE "PostAddress"
ADD
    CONSTRAINT "PostAddress_addressId_fkey" FOREIGN KEY ("addressId") REFERENCES "Address"("id") ON DELETE
SET NULL ON UPDATE CASCADE;

-- AddForeignKey

ALTER TABLE "Notification"
ADD
    CONSTRAINT "Notification_userId_fkey" FOREIGN KEY ("userId") REFERENCES "User"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey

ALTER TABLE "Message"
ADD
    CONSTRAINT "Message_userId_fkey" FOREIGN KEY ("userId") REFERENCES "User"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey

ALTER TABLE "Message"
ADD
    CONSTRAINT "Message_conversationId_fkey" FOREIGN KEY ("conversationId") REFERENCES "Conversation"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey

ALTER TABLE "Address"
ADD
    CONSTRAINT "Address_parentId_fkey" FOREIGN KEY ("parentId") REFERENCES "Address"("id") ON DELETE
SET NULL ON UPDATE CASCADE;

-- AddForeignKey

ALTER TABLE "_CategoryToUser"
ADD
    CONSTRAINT "_CategoryToUser_A_fkey" FOREIGN KEY ("A") REFERENCES "Category"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey

ALTER TABLE "_CategoryToUser"
ADD
    CONSTRAINT "_CategoryToUser_B_fkey" FOREIGN KEY ("B") REFERENCES "User"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey

ALTER TABLE
    "_ConversationToUser"
ADD
    CONSTRAINT "_ConversationToUser_A_fkey" FOREIGN KEY ("A") REFERENCES "Conversation"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey

ALTER TABLE
    "_ConversationToUser"
ADD
    CONSTRAINT "_ConversationToUser_B_fkey" FOREIGN KEY ("B") REFERENCES "User"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey

ALTER TABLE "_FileToMessage"
ADD
    CONSTRAINT "_FileToMessage_A_fkey" FOREIGN KEY ("A") REFERENCES "File"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey

ALTER TABLE "_FileToMessage"
ADD
    CONSTRAINT "_FileToMessage_B_fkey" FOREIGN KEY ("B") REFERENCES "Message"("id") ON DELETE CASCADE ON UPDATE CASCADE;