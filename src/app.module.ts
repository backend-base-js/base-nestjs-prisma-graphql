import { ApolloDriver, ApolloDriverConfig } from '@nestjs/apollo';
import { BullModule } from '@nestjs/bull';
import { CacheModule, Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { GraphQLModule } from '@nestjs/graphql';
import redisStore from 'cache-manager-ioredis';
import { join } from 'path';
import { AppConfig } from './config/type';
import { validate } from './config/validation';
import { AddressModule } from './modules/address/address.module';
import { DataloaderModule } from './modules/common/dataloader/dataloader.module';
import { FirebaseModule } from './modules/common/firebase/firebase.module';
import { CloudinaryModule } from './modules/file/cloudinary/cloudinary.module';
import { FileModule } from './modules/file/file.module';
import { NotificationModule } from './modules/notification/notification.module';
import { PostAddressModule } from './modules/post-address/post-address.module';
import { PostModule } from './modules/post/post.module';
import { UserModule } from './modules/user/user.module';
import { ComplexityPlugin } from './utils/plugins/complexity.plugin';
@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      cache: true,
      expandVariables: true,
      validate
    }),
    CacheModule.registerAsync({
      isGlobal: true,
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: async (configService: ConfigService<AppConfig>) => ({
        store: redisStore,
        host: configService.get<string>('REDIS_HOST'),
        port: configService.get<number>('REDIS_PORT')
      })
    }),
    GraphQLModule.forRootAsync<ApolloDriverConfig>({
      driver: ApolloDriver,
      useFactory: async (): Promise<ApolloDriverConfig> => {
        return {
          debug: true,
          playground: true,
          autoSchemaFile: join(process.cwd(), 'schema.gql')
        };
      }
    }),
    BullModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (configService: ConfigService<AppConfig>) => {
        return {
          redis: {
            host: configService.get('REDIS_HOST'),
            port: configService.get('REDIS_PORT')
          }
        };
      }
    }),
    PostModule,
    UserModule,
    CloudinaryModule,
    FileModule,
    FirebaseModule,
    DataloaderModule,
    PostAddressModule,
    NotificationModule,
    AddressModule
  ],
  providers: [ComplexityPlugin]
})
export class AppModule {}
